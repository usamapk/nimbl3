//
//  Extensions.swift
//  Nimbl3
//
//  Created by Usama Abdul Aziz on 28/10/2017.
//  Copyright © 2017 usamaabdulaziz. All rights reserved.
//

import UIKit

extension UIImage {
    
    class func imageFromColor(_ color: UIColor) -> UIImage? {
        let rectangle = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContextWithOptions(rectangle.size, false, 0)
        color.setFill()
        UIRectFill(rectangle)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
}

extension UIColor {
    
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }

}


extension UINavigationController {
    
    func overrideColor(_ color: UIColor) {
        navigationBar.isTranslucent = true
        navigationBar.setBackgroundImage(UIImage.imageFromColor(color), for: .default)
    }
    
}

extension UIView {
    func pop() {
        transform = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
        UIView.animate(withDuration: 0.3/1.5, animations: {
            self.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
        }, completion: { _ in
            UIView.animate(withDuration: 0.3/2, animations: {
                self.transform = CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9)
            }, completion: { _ in
                UIView.animate(withDuration: 0.3/2, animations: {
                    self.transform = CGAffineTransform.identity
                })
            })
        })
    }
}
