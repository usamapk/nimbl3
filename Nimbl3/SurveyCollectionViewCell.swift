//
//  SurveyCollectionViewCell.swift
//  Nimbl3
//
//  Created by Usama Abdul Aziz on 29/10/2017.
//  Copyright © 2017 usamaabdulaziz. All rights reserved.
//

import UIKit

class SurveyCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
}
