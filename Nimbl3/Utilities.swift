//
//  Utilities.swift
//  Nimbl3
//
//  Created by Usama Abdul Aziz on 28/10/2017.
//  Copyright © 2017 usamaabdulaziz. All rights reserved.
//

import UIKit
import KeychainSwift

class Utilities {
    
    class func getUserAccessToken() -> String? {
        return KeychainSwift().get("accessToken98")
    }
    
    class func saveUserAccessToken(_ token: String?) {
        KeychainSwift().set(token ?? "", forKey: "accessToken98")
    }
    
}
