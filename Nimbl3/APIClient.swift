//
//  APIClient.swift
//  Nimbl3
//
//  Created by Usama Abdul Aziz on 28/10/2017.
//  Copyright © 2017 usamaabdulaziz. All rights reserved.
//

import Foundation
import Alamofire
import MBProgressHUD

enum APIClient: URLRequestConvertible {
    
    static let manager: SessionManager = {
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForResource = 3600
        manager.session.configuration.timeoutIntervalForRequest = 3600
        return manager
    }()
    
    var APIEndpoint: URL! {
        return URL(string: "https://nimbl3-survey-api.herokuapp.com")
    }
    
    case getSurveys
    indirect case refreshToken(request: APIClient, showLoader: Bool, successBlock: ((Any) -> Void)?, failureBlock: (([String], Bool) -> Void)?)
    
    func asURLRequest() throws -> URLRequest {
        
        let relativePath: String = {
            switch self {
            case .getSurveys:
                return "surveys.json"
            case .refreshToken:
                return "oauth/token"
            }
        }()
        
        let method: HTTPMethod = {
            switch self {
            case .getSurveys:
                return .get
            case .refreshToken:
                return .post
            }
        }()
        
        let parameters: ([String: Any]) = {
            switch self {
            case .getSurveys:
                return ["access_token": Utilities.getUserAccessToken() ?? ""]
            case .refreshToken:
                return ["grant_type":"password", "username":"carlos@nimbl3.com", "password":"antikera"]
            }
        }()
        
        let encoding: ParameterEncoding = {
            switch method {
            case .post:
                return JSONEncoding()
            default:
                return URLEncoding()
            }
        }()
        
        
        var urlRequest = URLRequest(url: URL(string: relativePath, relativeTo: APIEndpoint)!)
        urlRequest.httpMethod = method.rawValue
        
        print("\n\n------- API_REQUEST [\(relativePath)]\nREQUEST_PARAMS: \(parameters)")
        return try encoding.encode(urlRequest, with: parameters)
    }
    
    
    
    static func callAPI(request: APIClient, showLoader: Bool = true, onSuccess successBlock: ((Any) -> Void)?, onFailure failureBlock: (([String], Bool) -> Void)?) {
        var HUD: MBProgressHUD?
        if showLoader {
            HUD = MBProgressHUD.showAdded(to: UIApplication.shared.windows.last!, animated: true)
            HUD?.backgroundView.color = UIColor.black.withAlphaComponent(0.4)
        }
        
        let responseCompletion: (DataResponse<Any>) -> Void = { (response) in
            
            let handleFailure: (_: [String], _: Any, _: Error?) -> Void = {errorArray, log, errorObject in
                print("FAILURE(\((response.response != nil) ? response.response!.statusCode : 0)) [\(response.request!.url!.relativePath)]: \(log)")
                switch response.result {
                case .success(let object):
                    print("Response Object: \(object)")
                default: break
                }
                var isNetworkError = false
                if let error = errorObject as NSError?, error.code == NSURLErrorNotConnectedToInternet {
                    isNetworkError = true
                }
                failureBlock?(errorArray, isNetworkError)
            }
            
            if showLoader {
                HUD?.hide(animated: true)
            }
            switch response.result {
            case .success(let dictionary):
                print("\(response)")
                successBlock?(dictionary)
                switch request {
                case .refreshToken(let originalRequest, let originalShowLoader, let originalSuccessBlock, let originalFailureBlock):
                    Utilities.saveUserAccessToken((dictionary as? [String: Any])?["access_token"] as? String)
                    callAPI(request: originalRequest, showLoader: originalShowLoader, onSuccess: originalSuccessBlock, onFailure: originalFailureBlock)
                default: break
                }
            case .failure(let error):
                print(error.localizedDescription)
                handleFailure([error.localizedDescription], "Unable to parse JSON.", error)
                if response.response?.statusCode == 401 {
                    callAPI(request: .refreshToken(request: request, showLoader: showLoader, successBlock: successBlock, failureBlock: failureBlock), onSuccess: nil, onFailure: nil)
                }
                else {
                    let messageHUD = MBProgressHUD.showAdded(to: UIApplication.shared.windows.last!, animated: true)
                    messageHUD.backgroundView.color = UIColor.black.withAlphaComponent(0.4)
                    messageHUD.mode = .text
                    messageHUD.detailsLabel.text = error.localizedDescription
                    messageHUD.hide(animated: true, afterDelay: 5.0)
                }
            }
        }
        Alamofire.request(request) .responseJSON(completionHandler: responseCompletion)
    }
}

