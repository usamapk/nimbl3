//
//  SurveyModel.swift
//  Nimbl3
//
//  Created by Usama Abdul Aziz on 29/10/2017.
//  Copyright © 2017 usamaabdulaziz. All rights reserved.
//

import UIKit

class SurveyModel {
    
    var surveyID: String
    var surveyTitle: String
    var surveyDescription: String
    var surveyCoverImageURL: URL
    
    
    init?(JSON: [String: Any]?) {
        guard let surveyID = JSON?["id"] as? String else { return nil }
        guard let surveyTitle = JSON?["title"] as? String else { return nil }
        guard let surveyDescription = JSON?["description"] as? String else { return nil }
        guard let urlString = JSON?["cover_image_url"] as? String, let surveyCoverImageURL = URL(string: "\(urlString)l") else { return nil }
        self.surveyID = surveyID
        self.surveyTitle = surveyTitle
        self.surveyDescription = surveyDescription
        self.surveyCoverImageURL = surveyCoverImageURL
    }
    
    class func array(fromDictionaryArray array: Any) -> [SurveyModel] {
        var modelArray = [SurveyModel]()
        guard let array = array as? [[String: Any]] else { return modelArray }
        for dictionary in array {
            if let model = SurveyModel(JSON: dictionary) {
                modelArray.append(model)
            }
        }
        return modelArray
    }
    
}
