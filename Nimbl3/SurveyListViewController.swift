//
//  SurveyListViewController.swift
//  Nimbl3
//
//  Created by Usama Abdul Aziz on 28/10/2017.
//  Copyright © 2017 usamaabdulaziz. All rights reserved.
//

import UIKit
import AlamofireImage

class SurveyListViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var pageControlCenterYConstraint: NSLayoutConstraint!
    @IBOutlet weak var pageControlMainView: UIView!
    @IBOutlet weak var takeSurveyButton: UIButton!
    
    var surveysListArray: [SurveyModel]?
    var gradientMaskLayer = CAGradientLayer()
    
    
    //MARK: - Overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        getData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        gradientMaskLayer.frame = pageControlMainView.bounds
        adjustPageControl(shouldAnimate: false)
    }
    
    //MARK: - API Call
    
    func getData() {
        APIClient.callAPI(request: .getSurveys, onSuccess: { (value) in
            self.surveysListArray = SurveyModel.array(fromDictionaryArray: value)
            if self.surveysListArray?.count ?? 0 > 1 {
                self.takeSurveyButton.isHidden = false
                self.takeSurveyButton.pop()
            }
            else {
                self.takeSurveyButton.isHidden = true
            }
            self.pageControl.numberOfPages = self.surveysListArray?.count ?? 0
            self.collectionView.setContentOffset(CGPoint.zero, animated: false)
            self.collectionView.reloadData()
        }, onFailure: nil)
    }
    
    //MARK: - Custom Methods
    
    func customisePageControl() {
        for bullet in pageControl.subviews {
            bullet.layer.borderColor = UIColor.white.cgColor
            bullet.layer.borderWidth = 0.4
        }
    }
    
    func adjustPageControl(shouldAnimate: Bool) {
        if pageControl.numberOfPages > 0 {
            customisePageControl()
            let fadedLength = pageControlMainView.frame.height * 0.1
            let firstPageControlBulletOrigin = abs(pageControl.subviews[0].frame.origin.x)
            let halfPageControlParent = pageControlMainView.frame.height/2
            let offset = firstPageControlBulletOrigin - halfPageControlParent + fadedLength
            if offset > 0 {
                let currentPageOriginX = pageControl.subviews[pageControl.currentPage].frame.origin.x + offset
                let pageOffset: CGFloat = {
                    if currentPageOriginX > 0 {
                        return -currentPageOriginX
                    }
                    else {
                        return 0
                    }
                }()
                self.pageControlCenterYConstraint.constant = offset + (pageOffset)
                if shouldAnimate {
                    UIView.animate(withDuration: 0.5, animations: {
                        self.pageControlMainView.layoutIfNeeded()
                    })
                }
            }
            else {
                pageControlCenterYConstraint.constant = 0
            }
            
        }
    }
    
    func setupViews() {
        navigationController?.overrideColor(UIColor.black.withAlphaComponent(0.6))
        pageControl.transform = CGAffineTransform(rotationAngle: .pi/2)
        
        gradientMaskLayer.colors = [UIColor.clear.cgColor, UIColor.black.cgColor, UIColor.black.cgColor, UIColor.clear.cgColor]
        gradientMaskLayer.locations = [0, 0.1, 0.9, 1]
        pageControlMainView.layer.mask = gradientMaskLayer
        
        takeSurveyButton.layer.cornerRadius = 20
        
    }
    
    //MARK: - IBActions
    
    @IBAction func refreshPressed() {
        getData()
    }
    
    
}

extension SurveyListViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return surveysListArray?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SurveyCell", for: indexPath) as! SurveyCollectionViewCell
        let model = surveysListArray![indexPath.item]
        cell.imageView.image = nil
        cell.imageView.af_setImage(withURL: model.surveyCoverImageURL) { (response) in
            switch response.result {
            case .success(let image) :
                cell.imageView.image = image
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        cell.titleLabel.text = model.surveyTitle
        cell.descriptionLabel.text = model.surveyDescription
        
        cell.titleLabel.alpha = 0
        cell.descriptionLabel.alpha = 0
        
        UIView.animate(withDuration: 1, animations: {
            cell.titleLabel.alpha = 1.0
            cell.descriptionLabel.alpha = 1.0
        })
        
        return cell
    }
    
}

extension SurveyListViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageHeight = collectionView.frame.size.height
        let fractionalPage = collectionView.contentOffset.y / pageHeight
        let page = lroundf(Float(fractionalPage))
        self.pageControl.currentPage = page
        adjustPageControl(shouldAnimate: true)
    }
    
}

